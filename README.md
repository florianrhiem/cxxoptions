# CXXOptions

[![license](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
[![build status](https://gitlab.com/florianrhiem/cxxoptions/badges/master/build.svg)](https://gitlab.com/florianrhiem/cxxoptions/pipelines)
[![coverage report](https://gitlab.com/florianrhiem/cxxoptions/badges/master/coverage.svg)](tests/tests.cxx)

A type-safe integral key / arbitrary value container, meant primarily for storing runtime options used by multiple classes.

## Installation

1. Download [options.hxx](options.hxx)
2. Include the header file in your code
3. Compile your code using a compiler that supports C++11

For examples, see [tests.cxx](tests/tests.cxx). The tests are compiled with clang++ 3.6, 3.7, 3.8 and g++ 4.7, 4.8, 4.9 and 5.x using the packages for Ubuntu 16.04 LTS.

## Getting started

Options are indentified by integral indices, either scoped enums or simple *int* values. While scoped enums provide additional safety against defining duplicate options, using simple *int* values allows you to spread the options across several header files.

    enum class ExampleOptionIndices {
        WIDTH,
        LABEL
    };

To use the *CXXOptions::Options* class, you need to define the possible options that can be stored, this is done by defining a struct template and specializing it:

    template<ExampleOptionIndices index>
    struct ExampleOption;

    template<>
    struct ExampleOption<ExampleOptionIndices::WIDTH> {
        float default_value = 2.5f;
    };

    template<>
    struct ExampleOption<ExampleOptionIndices::LABEL> {
        std::string default_value = "";
    };

Now you can use the *Options* template, though you might want to create a new name for the specialized template:

    using ExampleOptions = ::CXXOptions::Options<ExampleOptionIndices, ExampleOption>;

You can then simply create instances of this Options class, set options, get options, etc:

    ExampleOptions options;
    auto width = options.get<ExampleOptionIndices::WIDTH>(); // 2.5 (default)
    options.set<ExampleOptionIndices::LABEL>("interesting data");
    auto label = options.get<ExampleOptionIndices::LABEL>(); // "interesting data"

Besides the getting and setting of single option values, you can also set several options at once. To do so, create a new Options object that contains the options you want to set:

    auto changed_options = ExampleOptions::withOptions<ExampleOptionIndices::WIDTH, ExampleOptionIndices::LABEL>(1.25, "bacon/spam");
    options.set(changed_options);

If you want to react to option changes, you can get the set of changed option indices either as return value of *options.set(changed_options)*, or you can call *changed_options.modifiedIndices()*. Even if you changed the value of an option to its default value, it will still be considered modified (after all you set it explicitly). To reset it, call *options.setToDefault<index>()* for a single option or call *options.clear()* to reset all options.

## Using int or a scoped enum for option indices

The option indices need to be some kind of integral constants, so it is up to you to use *int*, an unscoped or a scoped *enum*. Scoped enums, declared using *enum class* have the benefit of additional type-safety, so they might be best for several cases. However, as *CXXOptions::Options* allows you to define options in various places, you might want to use *int* and several unscoped *enums*, as the scoped *enum* would force you to again have one central place where all option indices would be defined.
