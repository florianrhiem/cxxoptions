#ifndef CXXOPTIONS_OPTIONS_HXX
#define CXXOPTIONS_OPTIONS_HXX

#include <set>
#include <map>
#include <memory>
#include <array>

namespace CXXOptions {
    
    /** Type-safe integral key - arbitrary value container, for use as storage for options.*/
    template<typename IndexT, template<IndexT> class OptionT>
    class Options {
    public:
        /** Utility type for easier access to the type of an option value */
        template<IndexT index>
        struct Type {
            typedef decltype(OptionT<index>().default_value) type;
        };

        /** Convenience typedef for the type used for option indices */
        typedef IndexT index_type;

        /** Creates an empty Options object. */
        Options();

        /** Creates an Options object with the options from another object. */
        Options(const Options &other);

        /** Creates an Options object with the given options already set. */
        template<IndexT index, IndexT... indices>
        static Options withOptions(typename Type<index>::type value, typename Type<indices>::type... values);

        virtual ~Options();

        /** Returns the value of this option, or its default value if it has not been set. */
        template<IndexT index>
        typename Type<index>::type get() const;

        /** Returns whether this option is set. */
        template<IndexT index>
        bool has() const;

        /** Sets options from another object. */
        std::set<IndexT> set(const Options &other);

        /** Sets the option to the given value. */
        template<IndexT index>
        void set(typename Type<index>::type value);

        /** Sets several options in the given order. For duplicate indices, the last value for any particular index is used. */
        template<IndexT index, IndexT... indices, typename enable = typename std::enable_if<sizeof...(indices) != 0>::type>
        void set(typename Type<index>::type value, typename Type<indices>::type... values);

        /** Resets the option to its default value. */
        template<IndexT... indices>
        void setToDefault();

        /** Resets all options to their default values. */
        void clear();

        /** Returns the set of modified option indices. This includes options set to the default value, unless it was done using setToDefault() or the options were reset using clear(). */
        std::set<IndexT> modifiedIndices() const;

    private:
        struct IStorableOption {
            virtual ~IStorableOption() {}
        };

        template<IndexT index>
        struct StorableOption;

        std::map<IndexT, std::shared_ptr<IStorableOption>> m_options;
    };

    template<typename IndexT, template<IndexT> class OptionT>
    template<IndexT index>
    struct Options<IndexT, OptionT>::StorableOption: public Options<IndexT, OptionT>::IStorableOption {
        StorableOption(const typename Type<index>::type& value) : m_value(value) {}
        typename Type<index>::type m_value;
    };

    template<typename IndexT, template<IndexT> class OptionT>
    Options<IndexT, OptionT>::Options() {
    }

    template<typename IndexT, template<IndexT> class OptionT>
    Options<IndexT, OptionT>::Options(const Options<IndexT, OptionT>& other) : m_options(other.m_options) {
    }

    template<typename IndexT, template<IndexT> class OptionT>
    template<IndexT index, IndexT... indices>
    Options<IndexT, OptionT> Options<IndexT, OptionT>::withOptions(typename Type<index>::type value, typename Type<indices>::type... values) {
        Options<IndexT, OptionT> options;
        options.set<index, indices...>(value, values...);
        return options;
    }

    template<typename IndexT, template<IndexT> class OptionT>
    Options<IndexT, OptionT>::~Options() {
    }

    template<typename IndexT, template<IndexT> class OptionT>
    template<IndexT index>
    typename Options<IndexT, OptionT>::template Type<index>::type Options<IndexT, OptionT>::get() const {
        if (has<index>()) {
            if (auto storableOption = static_cast<StorableOption<index> *>(m_options.at(index).get())) {
                return storableOption->m_value;
            }
        }
        return OptionT<index>().default_value;
    }

    template<typename IndexT, template<IndexT> class OptionT>
    template<IndexT index>
    bool Options<IndexT, OptionT>::has() const {
        return (m_options.find(index) != m_options.end());
    }

    template<typename IndexT, template<IndexT> class OptionT>
    std::set<IndexT> Options<IndexT, OptionT>::modifiedIndices() const {
        std::set<IndexT> indices;
        for (const auto& index_option : m_options) {
            indices.insert(index_option.first);
        }
        return indices;
    }

    template<typename IndexT, template<IndexT> class OptionT>
    template<IndexT index>
    void Options<IndexT, OptionT>::set(typename Type<index>::type value) {
        m_options[index] = std::make_shared<StorableOption<index>>(value);
    }

    template<typename IndexT, template<IndexT> class OptionT>
    template<IndexT index, IndexT... indices, typename enable>
    void Options<IndexT, OptionT>::set(typename Type<index>::type value, typename Type<indices>::type... values) {
        set<index>(value);
        set<indices...>(values...);
    }

    template<typename IndexT, template<IndexT> class OptionT>
    template<IndexT... indices>
    void Options<IndexT, OptionT>::setToDefault() {
        std::array<IndexT, sizeof...(indices)> indices_array = {{indices...}};
        for (const IndexT& index : indices_array) {
            m_options.erase(index);
        }
    }

    template<typename IndexT, template<IndexT> class OptionT>
    std::set<IndexT> Options<IndexT, OptionT>::set(const Options &other) {
        std::set<IndexT> updated_indices;
        for (const auto& index_option : other.m_options) {
            const auto& index = index_option.first;
            const auto& option = index_option.second;
            updated_indices.insert(index);
            m_options[index] = option;
        }
        return updated_indices;
    }

    template<typename IndexT, template<IndexT> class OptionT>
    void Options<IndexT, OptionT>::clear() {
        m_options.clear();
    }

}

#endif
