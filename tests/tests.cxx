/**
 * Examples for automated testing.
 */
#include <options.hxx>
#include <string>
#include <iostream>

#include "tests/util.hxx"

namespace Test {
    enum class OptionIDs {
        PI,
        E
    };

    template<OptionIDs index>
    struct Option;

    using Options = ::CXXOptions::Options<OptionIDs, Option>;

    template<>
    struct Option<OptionIDs::PI> {
        float default_value = 3.14;
    };
    template<>
    struct Option<OptionIDs::E> {
        std::string default_value = "e";
    };
}

void emptyOptions() {
    const Test::Options options;
    ASSERT_EQUALS(options.modifiedIndices().size(), 0u);
    ASSERT_FALSE(options.has<Test::OptionIDs::E>());
    ASSERT_FALSE(options.has<Test::OptionIDs::PI>());
}

void copyConstructor() {
    const auto options1 = Test::Options::withOptions<Test::OptionIDs::E>("a");
    const auto options2(options1);
    ASSERT_EQUALS(options2.modifiedIndices().size(), 1u);
    ASSERT_TRUE(options2.has<Test::OptionIDs::E>());
    ASSERT_EQUALS(options2.get<Test::OptionIDs::E>(), "a");
    ASSERT_FALSE(options2.has<Test::OptionIDs::PI>());
}

void copyOptions() {
    const auto options1 = Test::Options::withOptions<Test::OptionIDs::E>("a");
    Test::Options options2;
    const auto result = options2.set(options1);
    ASSERT_EQUALS(options2.modifiedIndices().size(), 1u);
    ASSERT_TRUE(options2.has<Test::OptionIDs::E>());
    ASSERT_EQUALS(options2.get<Test::OptionIDs::E>(), "a");
    ASSERT_FALSE(options2.has<Test::OptionIDs::PI>());
    ASSERT_EQUALS(result.size(), 1u);
    ASSERT_EQUALS(result.count(Test::OptionIDs::E), 1u);
    ASSERT_EQUALS(result.count(Test::OptionIDs::PI), 0u);
}

void withOption() {
    const auto options = Test::Options::withOptions<Test::OptionIDs::E>("a");
    ASSERT_EQUALS(options.modifiedIndices().size(), 1u);
    ASSERT_TRUE(options.has<Test::OptionIDs::E>());
    ASSERT_EQUALS(options.get<Test::OptionIDs::E>(), "a");
    ASSERT_FALSE(options.has<Test::OptionIDs::PI>());
}

void withOptions() {
    const auto options = Test::Options::withOptions<Test::OptionIDs::E, Test::OptionIDs::PI>("a", 5.4321);
    ASSERT_EQUALS(options.modifiedIndices().size(), 2u);
    ASSERT_TRUE(options.has<Test::OptionIDs::E>());
    ASSERT_EQUALS(options.get<Test::OptionIDs::E>(), "a");
    ASSERT_TRUE(options.has<Test::OptionIDs::PI>());
    ASSERT_EQUALS(options.get<Test::OptionIDs::PI>(), 5.4321f);
}

void setOption() {
    Test::Options options;
    options.set<Test::OptionIDs::E>("a");
    ASSERT_EQUALS(options.modifiedIndices().size(), 1u);
    ASSERT_TRUE(options.has<Test::OptionIDs::E>());
    ASSERT_EQUALS(options.get<Test::OptionIDs::E>(), "a");
    ASSERT_FALSE(options.has<Test::OptionIDs::PI>());
}

void setOptions() {
    Test::Options options;
    options.set<Test::OptionIDs::E, Test::OptionIDs::PI>("a", 5.4321);
    ASSERT_EQUALS(options.modifiedIndices().size(), 2u);
    ASSERT_TRUE(options.has<Test::OptionIDs::E>());
    ASSERT_EQUALS(options.get<Test::OptionIDs::E>(), "a");
    ASSERT_TRUE(options.has<Test::OptionIDs::PI>());
    ASSERT_EQUALS(options.get<Test::OptionIDs::PI>(), 5.4321f);
}

void modifiedIndices() {
    Test::Options options;
    auto indices1 = options.modifiedIndices();
    ASSERT_EQUALS(indices1.size(), 0u);
    ASSERT_EQUALS(indices1.count(Test::OptionIDs::E), 0u);
    ASSERT_EQUALS(indices1.count(Test::OptionIDs::PI), 0u);
    options.set<Test::OptionIDs::E>("a");
    auto indices2 = options.modifiedIndices();
    ASSERT_EQUALS(indices2.size(), 1u);
    ASSERT_EQUALS(indices2.count(Test::OptionIDs::E), 1u);
    ASSERT_EQUALS(indices2.count(Test::OptionIDs::PI), 0u);
}

void defaultValues() {
    const Test::Options options;
    ASSERT_EQUALS(options.get<Test::OptionIDs::E>(), "e");
    ASSERT_EQUALS(options.get<Test::OptionIDs::PI>(), 3.14f);
}

void setToDefault() {
    Test::Options options;
    options.set<Test::OptionIDs::E>("a");
    ASSERT_EQUALS(options.modifiedIndices().size(), 1u);
    ASSERT_EQUALS(options.get<Test::OptionIDs::E>(), "a");
    options.setToDefault<Test::OptionIDs::E>();
    ASSERT_EQUALS(options.modifiedIndices().size(), 0u);
    ASSERT_EQUALS(options.get<Test::OptionIDs::E>(), "e");
}

void clear() {
    auto options = Test::Options::withOptions<Test::OptionIDs::E, Test::OptionIDs::PI>("a", 5.4321);
    ASSERT_EQUALS(options.modifiedIndices().size(), 2u);
    ASSERT_EQUALS(options.get<Test::OptionIDs::E>(), "a");
    ASSERT_EQUALS(options.get<Test::OptionIDs::PI>(), 5.4321f);
    options.clear();
    ASSERT_EQUALS(options.modifiedIndices().size(), 0u);
    ASSERT_EQUALS(options.get<Test::OptionIDs::E>(), "e");
    ASSERT_EQUALS(options.get<Test::OptionIDs::PI>(), 3.14f);
}

int main(void) {
    EXECUTE_TEST(emptyOptions);
    EXECUTE_TEST(copyConstructor);
    EXECUTE_TEST(copyOptions);
    EXECUTE_TEST(withOption);
    EXECUTE_TEST(withOptions);
    EXECUTE_TEST(setOption);
    EXECUTE_TEST(setOptions);
    EXECUTE_TEST(modifiedIndices);
    EXECUTE_TEST(defaultValues);
    EXECUTE_TEST(setToDefault);
    EXECUTE_TEST(clear);
    TEST_SUMMARY();
    return 0;
}
