#ifndef CXXOPTIONS_UTIL_HXX
#define CXXOPTIONS_UTIL_HXX

#include <sstream>

#define EXECUTE_TEST(test) TestUtils::executeTest((test), #test, __FILE__)
#define TEST_SUMMARY TestUtils::testSummary
#define ASSERT_EQUALS(left, right) TestUtils::assertEquals((left), (right), #left, #right, __FILE__, __LINE__)
#define ASSERT_TRUE(value) TestUtils::assertTrue((value), #value, __FILE__, __LINE__)
#define ASSERT_FALSE(value) TestUtils::assertFalse((value), #value, __FILE__, __LINE__)

namespace TestUtils {
    
    static int num_tests_run = 0;
    static int num_tests_failed = 0;
    
    class AssertionFailure : public std::exception {
    public:
        AssertionFailure(const std::string& message) : m_message(message) {
        }
        const char* what() const noexcept {
            return m_message.c_str();
        }
    private:
        std::string m_message;
    };

    template<typename LeftT, typename RightT>
    void assertEquals(const LeftT& left, const RightT& right, const std::string& left_name, const std::string& right_name, std::string file_name, int line_number) throw(AssertionFailure) {
        if (left == right) {
            return;
        }
        std::stringstream message;
        message << "\033[1m" << file_name << ":" << line_number << ": \033[31massertion failed\033[0m" << std::endl;
        message << "Expected " << left_name << " == " << right_name << "," << std::endl;
        message << "but " << left_name << " is '" << left << "'" << std::endl;
        message << "and " << right_name << " is '" << right << "'." << std::endl;
        throw AssertionFailure(message.str());
    }

    inline void assertTrue(const bool& value, const std::string& value_name, std::string file_name, int line_number) throw(AssertionFailure) {
        if (value) {
            return;
        }
        std::stringstream message;
        message << "\033[1m" << file_name << ":" << line_number << ": \033[31massertion failed\033[0m" << std::endl;
        message << "Expected " << value_name << " == true." << std::endl;
        throw AssertionFailure(message.str());
    }

    inline void assertFalse(const bool& value, const std::string& value_name, std::string file_name, int line_number) throw(AssertionFailure) {
        if (!value) {
            return;
        }
        std::stringstream message;
        message << "\033[1m" << file_name << ":" << line_number << ": \033[31massertion failed\033[0m" << std::endl;
        message << "Expected " << value_name << " == false." << std::endl;
        throw AssertionFailure(message.str());
    }

    inline void executeTest(const std::function<void()>& test, const std::string& test_name, const std::string& file_name) {
        num_tests_run += 1;
        std::cerr << "\033[1m" << file_name << ":" << test_name << "():";
        try {
            test();
            std::cerr << " \033[32mpassed\033[0m" << std::endl;
        } catch (const TestUtils::AssertionFailure& e) {
            num_tests_failed += 1;
            std::cerr << " \033[31mfailed\033[0m" << std::endl;
            std::cerr << e.what() << std::endl;
        }
    }

    inline void testSummary() {
        std::cerr << "Passed " << (TestUtils::num_tests_run - TestUtils::num_tests_failed) << "/" << TestUtils::num_tests_run << " tests." << std::endl;
        if (num_tests_failed) {
            exit(1);
        } else {
            exit(0);
        }
    }
}



#endif
